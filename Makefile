SRC= prj.cpp
BIN=prj
CXXFLAGS= -std=c++11 -O0 -g -Wall
CXX=g++
all: $(BIN)

$(BIN): $(SRC)
	$(CXX) $(CXXFLAGS) $(SRC) -o $(BIN)
clean:
	rm $(BIN)
