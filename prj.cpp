#include <stdio.h>
#include <algorithm>
#include <queue>
#include <set>
#include <cassert>

struct projekt {
	int liczbaProgramistow;
	std::set<int> zaleznosci;
	std::set<int> zalezyOd;
};

int main() {
	int liczbaPrj, liczbaZaleznosci, doUzbierania;
	scanf("%i%i%i", &liczbaPrj, &liczbaZaleznosci, &doUzbierania);
	std::priority_queue<std::pair <int, int>, std::vector<std::pair<int, int> >, std::greater<std::pair<int, int>  > > kolejka;
	projekt *tablicaProjektow = new projekt[liczbaPrj + 1];
	for (int i = 1; i <= liczbaPrj; i++) {
		scanf("%i", &tablicaProjektow[i].liczbaProgramistow);
	}
	for (int i = 0; i < liczbaZaleznosci; i++) {
		int a, b;
		scanf("%i%i", &a, &b);
		tablicaProjektow[b].zaleznosci.insert(a);
		tablicaProjektow[a].zalezyOd.insert(b);
	}
	int programisci = 0;
	for (int i = 1; i <= liczbaPrj; i++) {
		if (tablicaProjektow[i].zalezyOd.empty()) {
			kolejka.push(std::make_pair(tablicaProjektow[i].liczbaProgramistow, i));
		}
	}
	int zrealizowane = 0;
	while (zrealizowane < doUzbierania) {
		std::pair<int, int> element;
		element = kolejka.top();
		kolejka.pop();
		zrealizowane++;
		programisci = std::max(programisci, element.first);
		for (auto a : tablicaProjektow[element.second].zaleznosci) {
			tablicaProjektow[a].zalezyOd.erase(element.second);
			if (tablicaProjektow[a].zalezyOd.empty()) {
				kolejka.push(std::make_pair(tablicaProjektow[a].liczbaProgramistow, a));
			}
		}
	}
	printf("%i\n", programisci);
	delete[] tablicaProjektow;
	return 0;
}
